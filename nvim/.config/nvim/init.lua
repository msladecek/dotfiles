local ensure_packer = function()
    local fn = vim.fn
    local install_path = fn.stdpath('data') .. '/site/pack/packer/start/packer.nvim'
    if fn.empty(fn.glob(install_path)) > 0 then
        fn.system({ 'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path })
        vim.cmd [[packadd packer.nvim]]
        return true
    end
    return false
end

local packer_bootstrap = ensure_packer()

vim.cmd [[
  augroup Packer
    autocmd!
    autocmd BufWritePost init.lua PackerCompile
  augroup end
]]

require('packer').startup(function(use)
    use 'wbthomason/packer.nvim'
    use 'ggandor/leap.nvim'
    use 'tpope/vim-fugitive'
    use 'tpope/vim-abolish'
    use 'numToStr/Comment.nvim'
    -- UI to select things (files, grep results, open buffers...)
    use { 'nvim-telescope/telescope.nvim', requires = { 'nvim-lua/plenary.nvim' } }
    use { 'nvim-telescope/telescope-fzf-native.nvim', run = 'make' }
    use 'nvim-lualine/lualine.nvim'
    use 'ellisonleao/gruvbox.nvim'
    -- Add indentation guides even on blank lines
    use 'lukas-reineke/indent-blankline.nvim'
    -- Highlight, edit, and navigate code using a fast incremental parsing library
    use { 'nvim-treesitter/nvim-treesitter', run = ':TSUpdate' }
    use 'nvim-treesitter/playground'
    use 'nvim-treesitter/nvim-treesitter-textobjects'
    use 'hrsh7th/nvim-cmp'
    use {
        'VonHeikemen/lsp-zero.nvim',
        requires = {
            -- LSP Support
            { 'neovim/nvim-lspconfig' },
            { 'williamboman/mason.nvim' },
            { 'williamboman/mason-lspconfig.nvim' },

            -- Autocompletion
            { 'hrsh7th/nvim-cmp' },
            { 'hrsh7th/cmp-buffer' },
            { 'hrsh7th/cmp-path' },
            { 'saadparwaiz1/cmp_luasnip' },
            { 'hrsh7th/cmp-nvim-lsp' },
            { 'hrsh7th/cmp-nvim-lua' },

            -- Snippets
            { 'L3MON4D3/LuaSnip' },
            { 'rafamadriz/friendly-snippets' },
        }
    }

    -- use {
    --     'gelguy/wilder.nvim',
    --     config = function()
    --         local wilder = require('wilder')
    --         wilder.setup({modes = {':', '/', '?'}})
    --         wilder.set_option('pipeline', {
    --             wilder.branch(
    --                 wilder.cmdline_pipeline(),
    --                 wilder.search_pipeline()
    --             ),
    --         })
    --         wilder.set_option('renderer', wilder.popupmenu_renderer({
    --             -- highlighter applies highlighting to the candidates
    --             highlighter = wilder.basic_highlighter(),
    --         }))
    --     end,
    -- }
    --
    use 'Olical/conjure'

    -- use { 'glacambre/firenvim', run = function() vim.fn['firenvim#install'](0) end }
    use 'mbbill/undotree'

    use 'bkad/CamelCaseMotion'

    use 'folke/zen-mode.nvim'
    use 'mg979/vim-visual-multi'

    -- use {
    --   "folke/noice.nvim",
    --   event = "VimEnter",
    --   config = function() require("noice").setup() end,
    --   requires = {
    --     -- if you lazy-load any plugin below, make sure to add proper `module="..."` entries
    --     "MunifTanjim/nui.nvim",
    --     "rcarriga/nvim-notify",
    --   }
    -- }
    if packer_bootstrap then
        require('packer').sync()
    end
end)

local lsp = require("lsp-zero")

lsp.preset("recommended")

lsp.ensure_installed({
    'tsserver',
    'eslint',
    'sumneko_lua',
    'rust_analyzer',
})

-- Fix Undefined global 'vim'
lsp.configure('sumneko_lua', {
    settings = {
        Lua = {
            diagnostics = {
                globals = { 'vim' }
            }
        }
    }
})

--Set highlight on search
vim.o.hlsearch = false

--disable side columns
vim.wo.number = false
vim.wo.signcolumn = "no"

--Enable mouse mode
vim.o.mouse = 'a'

--Enable break indent
vim.o.breakindent = true

--Save undo history
vim.opt.undofile = true

--Case insensitive searching UNLESS /C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true

--Decrease update time
vim.o.updatetime = 250

-- Share system clipboard
vim.o.clipboard = 'unnamedplus'

--Set colorscheme
vim.o.termguicolors = true
vim.o.background = "dark"
require("gruvbox").setup({
    contrast = "hard"
})
vim.cmd([[colorscheme gruvbox]])

-- Set completeopt to have a better completion experience
vim.o.completeopt = 'menuone,noselect'

--Set statusbar
require('lualine').setup {
    options = {
        icons_enabled = false,
        theme = 'gruvbox',
        component_separators = '|',
        section_separators = '',
    },
    sections = {
        lualine_b = { 'filename' },
        lualine_c = {},
    }
}

-- Enable Comment.nvim
require('Comment').setup()

-- vim.g['conjure#debug'] = true
vim.g['conjure#client#clojure#nrepl#connection#auto_repl#cmd'] = "bb nrepl-server localhost:1667"
vim.g['conjure#client#python#stdio#command'] = 'ipython -i'

-- Remap space as leader key
local map = vim.api.nvim_set_keymap
map('', '<Space>', '<Nop>', { noremap = true, silent = true })
vim.g.mapleader = ' '
vim.g.maplocalleader = ','

-- Remap for dealing with word wrap
map('n', 'k', "v:count == 0 ? 'gk' : 'k'", { noremap = true, expr = true, silent = true })
map('n', 'j', "v:count == 0 ? 'gj' : 'j'", { noremap = true, expr = true, silent = true })

-- Highlight on yank
-- vim.cmd [[
--   augroup YankHighlight
--     autocmd!
--     autocmd TextYankPost * silent! lua vim.highlight.on_yank()
--   augroup end
-- ]]

-- Map blankline
vim.g.indent_blankline_char = '┊'
vim.g.indent_blankline_filetype_exclude = { 'help', 'packer' }
vim.g.indent_blankline_buftype_exclude = { 'terminal', 'nofile' }
vim.g.indent_blankline_show_trailing_blankline_indent = false

-- Telescope
local actions = require("telescope.actions")
require('telescope').setup {
    defaults = {
        mappings = {
            i = {
                ['<C-u>'] = false,
                ['<C-d>'] = false,
                ['<C-j>'] = actions.move_selection_next,
                ['<C-k>'] = actions.move_selection_previous,
            },
        },
    },
}

-- Enable telescope fzf native
require('telescope').load_extension 'fzf'
map('n', '<leader>bb', [[<cmd>lua require('telescope.builtin').buffers()<CR>]], { noremap = true, silent = true })
map('n', '<leader>ff', [[<cmd>lua require('telescope.builtin').find_files({previewer = false})<CR>]],
{ noremap = true, silent = true })
map('n', '<leader>/', [[<cmd>lua require('telescope.builtin').current_buffer_fuzzy_find()<CR>]],
{ noremap = true, silent = true })
map('n', '<leader>sh', [[<cmd>lua require('telescope.builtin').help_tags()<CR>]], { noremap = true, silent = true })
map('n', '<leader>st', [[<cmd>lua require('telescope.builtin').tags()<CR>]], { noremap = true, silent = true })
map('n', '<leader>sd', [[<cmd>lua require('telescope.builtin').grep_string()<CR>]], { noremap = true, silent = true })
map('n', '<leader>sp', [[<cmd>lua require('telescope.builtin').live_grep()<CR>]], { noremap = true, silent = true })
map('n', '<leader>so', [[<cmd>lua require('telescope.builtin').tags{ only_current_buffer = true }<CR>]],
{ noremap = true, silent = true })
map('n', '<leader>?', [[<cmd>lua require('telescope.builtin').oldfiles()<CR>]], { noremap = true, silent = true })

-- wildmenu
vim.cmd([[set wildmenu]])
vim.cmd([[set wildmode=longest,list,full]])

-- default indent config
vim.opt.tabstop = 4
vim.opt.shiftwidth = 4
vim.opt.softtabstop = 4
vim.opt.expandtab = true

-- Remap for window navigation
map('n', '<C-M-j>', '<C-w><C-j>', { noremap = true, silent = true })
map('n', '<C-M-k>', '<C-w><C-k>', { noremap = true, silent = true })
map('n', '<C-M-h>', '<C-w><C-h>', { noremap = true, silent = true })
map('n', '<C-M-l>', '<C-w><C-l>', { noremap = true, silent = true })
map('n', '<leader>w=', '<C-w>=', { noremap = true, silent = true })

-- muscle memory keybindings from doom emacs
map('n', '<leader>wd', ':close<cr>', { noremap = true, silent = true })
map('n', '<leader>bd', ':q<cr>', { noremap = true, silent = true })
map('n', '<leader>gg', ':Git<cr>', { noremap = true, silent = true })

-- leap
require('leap').add_default_mappings()
vim.keymap.set('x', 'x', 'x')

-- undotree
map('n', '<leader>au', ':UndotreeToggle<cr>', { noremap = true, silent = true })

-- camelcase word navigation
map('n', 'w', '<Plug>CamelCaseMotion_w', { silent = true })
map('n', 'b', '<Plug>CamelCaseMotion_b', { silent = true })
map('n', 'e', '<Plug>CamelCaseMotion_e', { silent = true })
map('n', 'ge', '<Plug>CamelCaseMotion_ge', { silent = true })

-- treesitter
-- Parsers must be installed manually via :TSInstall
require('nvim-treesitter.configs').setup {
    highlight = {
        enable = true, -- false will disable the whole extension
    },
    incremental_selection = {
        enable = true,
        keymaps = {
            init_selection = 'gnn',
            node_incremental = 'grn',
            scope_incremental = 'grc',
            node_decremental = 'grm',
        },
    },
    indent = {
        enable = true,
    },
    textobjects = {
        select = {
            enable = true,
            lookahead = true, -- Automatically jump forward to textobj, similar to targets.vim
            keymaps = {
                -- You can use the capture groups defined in textobjects.scm
                ['af'] = '@function.outer',
                ['if'] = '@function.inner',
                ['ac'] = '@class.outer',
                ['ic'] = '@class.inner',
            },
        },
        move = {
            enable = true,
            set_jumps = true, -- whether to set jumps in the jumplist
            goto_next_start = {
                [']m'] = '@function.outer',
                [']]'] = '@class.outer',
            },
            goto_next_end = {
                [']M'] = '@function.outer',
                [']['] = '@class.outer',
            },
            goto_previous_start = {
                ['[m'] = '@function.outer',
                ['[['] = '@class.outer',
            },
            goto_previous_end = {
                ['[M'] = '@function.outer',
                ['[]'] = '@class.outer',
            },
        },
    },
}

-- LSP settings
require("mason").setup({
    ui = {
        icons = {
            package_installed = "✓",
            package_pending = "➜",
            package_uninstalled = "✗"
        }
    }
})

local lspconfig = require('lspconfig')
map('n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', { noremap = true, silent = true })
map('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', { noremap = true, silent = true })
map('n', 'gD', '<cmd>lua vim.lsp.buf.references()<CR>', { noremap = true, silent = true })
map('n', '<leader>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', { noremap = true, silent = true })
map('n', '<leader>cr', '<cmd>lua vim.lsp.buf.rename()<CR>', { noremap = true, silent = true })
map('n', '<localleader>/', [[<cmd>lua require('telescope.builtin').lsp_document_symbols()<CR>]],
{ noremap = true, silent = true })
map('n', '<leader>cwa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', { noremap = true, silent = true })
map('n', '<leader>cwr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', { noremap = true, silent = true })
vim.cmd [[ command! Format execute 'lua vim.lsp.buf.formatting()' ]]

-- nvim-cmp supports additional completion capabilities
local cmp = require('cmp')
cmp.setup({
    mapping = {
        ['<C-j>'] = cmp.mapping.select_next_item({ behavior = cmp.SelectBehavior.Insert }),
        ['<C-k>'] = cmp.mapping.select_prev_item({ behavior = cmp.SelectBehavior.Insert }),
        ['<C-b>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
        ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
        ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
        ['<C-y>'] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
        ['<C-e>'] = cmp.mapping({
            i = cmp.mapping.abort(),
            c = cmp.mapping.close(),
        }),
        ['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    },
    sources = cmp.config.sources({
        { name = 'nvim_lsp' },
    }, {
        { name = 'buffer' },
    })
})

-- Set configuration for specific filetype.
cmp.setup.filetype('gitcommit', {
    sources = cmp.config.sources({
        { name = 'cmp_git' }, -- You can specify the `cmp_git` source if you were installed it.
    }, {
        { name = 'buffer' },
    })
})

-- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline('/', {
    sources = {
        { name = 'buffer' }
    }
})

-- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
cmp.setup.cmdline(':', {
    sources = cmp.config.sources({
        { name = 'path' }
    }, {
        { name = 'cmdline' }
    })
})

-- Setup lspconfig.
local capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())
-- Replace <YOUR_LSP_SERVER> with each lsp server you've enabled.
require('lspconfig')['lua_ls'].setup {
    capabilities = capabilities
}
-- require('lspconfig')['pyright'].setup()
