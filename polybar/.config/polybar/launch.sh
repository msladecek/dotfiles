#!/bin/bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -x polybar >/dev/null; do sleep 1; done

PRIMARY_SCREEN=eDP-1 polybar main &

for a in {1..3}
do
    SECONDARY_SCREEN=DP-$a polybar secondary &
    for b in {1..3}
    do
        SECONDARY_SCREEN=DP-$a-$b polybar secondary &
    done
done

echo "Bars launched..."
