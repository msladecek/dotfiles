import pdb

class Config(pdb.DefaultConfig):
    prompt = "(debug) "
    sticky_by_default = True
