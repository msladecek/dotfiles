#!/usr/bin/bash

blur_and_overlay () {
    scrot /tmp/screen.png
    icon="$HOME/.config/i3/locks/lock.png"
    tmpbg="/tmp/screen.png"

    (( $# )) && { icon=$1; }

    w=$(identify -format '%w' $icon)
    h=$(identify -format '%h' $icon)

    convert "$tmpbg" -scale 10% -scale 1000% "$tmpbg"

    xrandr | grep -w 'connected' | \
        sed -r 's/.*\b([0-9]*)x([0-9]*)\+([0-9]*)\+([0-9]*)\b.*/\1 \2 \3 \4/g' | \
        awk -v w=$w -v h=$h '{print "+" $3 + ($1 - w)/2 "+" $4 + ($2 - h)/2}' | \
        while read -r c; do
            convert "$tmpbg" "$icon" -geometry $c -composite -matte "$tmpbg"
        done

    i3lock -u -i "$tmpbg"
    rm "$tmpbg"
}

windows_green () {
    i3lock -c 008080
}

amixer set Master mute && \
    mpc pause && \
    windows_green && \
    amixer set Master toggle && \
    exec pkill -SIGRTMIN+10 i3block
