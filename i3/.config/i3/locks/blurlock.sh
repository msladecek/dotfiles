#!/bin/bash

SCROT=/tmp/lockscrot.png
BLUR=/tmp/lockblur.png

BLURTYPE="0x5"
#BLURTYPE="0x2"
#BLURTYPE="5x3"
#BLURTYPE="2x8"
#BLURTYPE="2x3"

fline=$(xrandr | grep "Screen 0:")
fx=$(echo $fline | sed -n "s/.*current \([0-9][0-9]*\)\>.*/\1/p")
fy=$(echo $fline | sed -n "s/.*current.*x \([0-9][0-9]*\)\>.*/\1/p")

nums=$(xrandr | grep primary | sed -n "s/.*\<\([0-9][0-9]*x[0-9][0-9]*+[0-9][0-9]*+[0-9][0-9]*\)\>.*/\1/p")
x=$(echo $nums | sed -n "s/^\([0-9][0-9]*\)x.*/\1/p")
y=$(echo $nums | sed -n "s/.*x\([0-9][0-9]*\)+.*/\1/p")
xoff=$(echo $nums | sed -n "s/.*+\([0-9][0-9]*\)+.*/\1/p")
yoff=$(echo $nums | sed -n "s/.*+\([0-9][0-9]*\)$/\1/p")

echo $nums

anno="+$(($xoff + $x/2 - $fx/2))+$(($yoff + $y/2 - $fy/2 + 220))"


scrot $SCROT
convert $SCROT -blur $BLURTYPE -fill white -undercolor '#00000080' -font Hermit -pointsize 160 -stroke black -strokewidth 3 -gravity Center -annotate $anno ' Locked ' $BLUR
i3lock -ef -i $BLUR
#feh $BLUR
rm $SCROT
rm $BLUR

