# msladecek's linux configuration
Nowdays running on Kubuntu + i3.

## Install

### Utilities
``` bash
sudo apt install git stow
```

### Clone repo
``` bash
$ git clone https://gitlab.com/msladecek/dotfiles ~/dotfiles
```

### Activate config for specific application e.g. i3
```bash
$ cd ~/dotfiles
$ stow i3
```

### Change default shell to fish
```bash
$ chsh --shell /usr/bin/fish martin
```
