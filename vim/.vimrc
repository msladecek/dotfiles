set nocompatible              " be iMproved, required
filetype off                  " required

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" The following are examples of different formats supported.
" Keep Plugin commands between vundle#begin/end.
" plugin on GitHub repo
Plugin 'bling/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'fneu/breezy'
Plugin 'morhetz/gruvbox'
"Plugin 'scrooloose/nerdcommenter'
"Plugin 'scrooloose/nerdtree'
Plugin 'rhysd/vim-clang-format'
"Plugin 'Valloric/YouCompleteMe'
"Plugin 'rdnetto/YCM-Generator'
Plugin 'nathanaelkane/vim-indent-guides'
Plugin 'guns/vim-clojure-static'
Plugin 'tpope/vim-fireplace'
Plugin 'kien/rainbow_parentheses.vim'
"Plugin 'davidhalter/jedi-vim.git'
"" Plugin 'tpope/vim-fugitive'
" plugin from http://vim-scripts.org/vim/scripts.html
"" Plugin 'L9'
" Git plugin not hosted on GitHub
"" Plugin 'git://git.wincent.com/command-t.git'
" git repos on your local machine (i.e. when working on your own plugin)
"" Plugin 'file:///home/gmarik/path/to/plugin'
" The sparkup vim script is in a subdirectory of this repo called vim.
" Pass the path to set the runtimepath properly.
"" Plugin 'rstacruz/sparkup', {'rtp': 'vim/'}
" Avoid a name conflict with L9
"" Plugin 'user/L9', {'name': 'newL9'}

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" To ignore plugin indent changes, instead use:
"filetype plugin on
"
" Brief help
" :PluginList       - lists configured plugins
" :PluginInstall    - installs plugins; append `!` to update or just :PluginUpdate
" :PluginSearch foo - searches for foo; append `!` to refresh local cache
" :PluginClean      - confirms removal of unused plugins; append `!` to auto-approve removal
"
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Color scheme
set t_Co=256
if has('gui_running')
    " GUI colors
    colorscheme github

    :set guioptions-=m  "remove menu bar
    :set guioptions-=T  "remove toolbar

    nnoremap <C-F1> :if &go=~#'m'<Bar>set go-=m<Bar>else<Bar>set go+=m<Bar>endif<CR>
    nnoremap <C-F2> :if &go=~#'T'<Bar>set go-=T<Bar>else<Bar>set go+=T<Bar>endif<CR>
    nnoremap <C-F3> :if &go=~#'r'<Bar>set go-=r<Bar>else<Bar>set go+
    set background=light
else
    " Non-GUI (terminal) colors
	"color msla3
    set background=dark
    set termguicolors
    "let g:airline_theme='gruvbox'
    let g:airline_theme='breezy'
    let g:gruvbox_contrast_dark='hard'
    "colorscheme breezy
    color gruvbox
endif


" Enable syntax highlighting
filetype on
filetype plugin indent on
syntax enable

" enter paste mode
set pastetoggle=<F2>

" Rebind <Leader> key
let mapleader = "\<Space>"

" Leader shortcut for saving and exiting
nnoremap <Leader>w :w<CR>
nnoremap <Leader>q :q<CR>

" make y behave normally
map Y y$

" use j and k to navigate wrapped lines
nnoremap j gj
nnoremap k gk

map J 10j
map K 10k

" use leader+m and n to navigate buffers
map <Leader>m :bnext<CR>
map <Leader>n :bprev<CR>

" write with sudo
command SudoWrite :w !sudo tee %

set cm=blowfish
set wildmenu
set wildmode=longest,list
set confirm
set ignorecase
set smartcase
set linebreak
set visualbell
set encoding=utf8
set clipboard=unnamedplus

"" splits
set splitbelow
set splitright
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>


" custom listchars
"set list
"set listchars=tab:▸\ ,eol:¬,trail:⋅,extends:❯,precedes:❮
"set showbreak=↪

"let g:indentLine_char = '︙'
" let g:indentLine_color_term = 237

" disable swap files
set nobackup
set nowritebackup
set noswapfile

" Showing line numbers and length
set number " show line numbers
set showmatch
set hlsearch
set incsearch
set showcmd
set ruler
set tw=79 " width of document (used by gd)
set nowrap " don't automatically wrap on load
set fo-=t " don't automatically wrap text when typing
set colorcolumn=80
highlight ColorColumn ctermbg=234
"set list

" Real programmers don't use TABs but spaces
set tabstop=4
set softtabstop=4
set shiftwidth=4
set shiftround
set expandtab
set smartindent

" set undodir
set undofile
set undodir=~/.vim/undodir

" indent guide settings
let g:indent_guides_auto_colors = 0
let g:indent_guides_start_level = 2
let g:indent_guides_guide_size = 1

autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd  guibg=red   ctermbg=235
autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=green ctermbg=234

noremap <F3> :IndentGuidesToggle<CR>

" let g:airline_powerline_fonts = 1
set laststatus=2
let g:airline_left_sep=''
let g:airline_right_sep=''
"let g:airline_theme='dark'

" let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#left_sep = ''
let g:airline#extensions#tabline#left_alt_sep = ''

set noshowmode
"let g:loaded_airline=1

" airline theme for gvim
if has('gui_running')
    :let g:airline_theme='base16'
endif

" clang formater
" let g:clang_format_fallback_style='~/.vim/.clang-format'

"map <C-K> :pyf /usr/share/clang/clang-format.py<cr>
"imap <C-K> <c-o>:pyf /usr/share/clang/clang-format.py<cr>

"" fireplace
map cpa :%Eval<CR>

"" rainbow parentheses
let g:rbpt_colorpairs = [
    \ ['brown',       'RoyalBlue3'],
    \ ['Darkblue',    'SeaGreen3'],
    \ ['darkgray',    'DarkOrchid3'],
    \ ['darkgreen',   'firebrick3'],
    \ ['darkcyan',    'RoyalBlue3'],
    \ ['darkred',     'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['brown',       'firebrick3'],
    \ ['gray',        'RoyalBlue3'],
    \ ['black',       'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['Darkblue',    'firebrick3'],
    \ ['darkgreen',   'RoyalBlue3'],
    \ ['darkcyan',    'SeaGreen3'],
    \ ['darkred',     'DarkOrchid3'],
    \ ['red',         'firebrick3'],
    \ ]

au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces


"" esc delay
set timeoutlen=1000 ttimeoutlen=0


:fixdel
set backspace=indent,eol,start

set completeopt-=preview
let g:ycm_global_ycm_extra_conf = "~/.vim/.ycm_extra_conf.py"
"let g:ycm_min_num_of_chars_for_completion = 1
let g:ycm_autoclose_preview_window_after_completion = 1
"let g:ycm_autoclose_preview_window_after_insertion = 1

" Press F4 to toggle highlighting on/off, and show current value.
noremap <Leader>h :set hlsearch! hlsearch?<CR>

""" Toggle relative/absolute numbering
function NumberToggle()
    if(&relativenumber == 1)
        set norelativenumber
    else
        set relativenumber
    endif
endfunc

nnoremap <F10> :call NumberToggle()<cr>

