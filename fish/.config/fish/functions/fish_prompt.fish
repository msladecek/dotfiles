set -g pad ""

## Function to show a segment
function prompt_segment -d "Function to show a segment"
    # Get colors
    set -l bg $argv[1]
    set -l fg $argv[2]

    # Set 'em
    set_color -b $bg
    if [ "$argv[4]" = bold ]
        set_color --bold $fg
    else
        set_color $fg
    end

    # Print text
    if [ -n "$argv[3]" ]
        echo -ne -s $argv[3]
    end
end

## Function to show current status
function show_status -d "Function to show the current status"
    if [ $RETVAL -ne 0 ]
        prompt_segment normal red "▶ $RETVAL\n"
        set pad ""
    end
    if [ -n "$SSH_CLIENT" ]
        prompt_segment blue white " SSH: "
        set pad ""
    end
end

function show_virtualenv -d "Show active python virtual environments"
    if set -q VIRTUAL_ENV
        set -l venvname (basename "$VIRTUAL_ENV")
        prompt_segment normal white "($venvname) "
    end
end

function show_nix_shell_info
    if test -n "$IN_NIX_SHELL"
        echo -n "<nix-shell> "
    end
end


## Show user if not in default users
function show_user -d "Show user"
    if not contains $USER $default_user; or test -n "$SSH_CLIENT"
        set -l host (hostname -s)
        set -l who (whoami)
        prompt_segment normal green "$who" bold

        # Skip @ bit if hostname == username
        if [ "$USER" != "$HOST" ]
            prompt_segment normal green "@$host " bold
            set pad ""
        end
    end
end

# Show directory
function show_pwd -d "Show the current directory"
    set -l pwd
    if [ (string match -r '^'"$VIRTUAL_ENV_PROJECT" $PWD) ]
        set pwd (string replace -r '^'"$VIRTUAL_ENV_PROJECT"'($|/)' '≫ $1' $PWD)
    else
        set pwd (prompt_pwd)
    end
    prompt_segment normal brblue "$pad$pwd" bold
end

# Show prompt w/ privilege cue
function show_prompt -d "Shows prompt with cue for current priv"
    set -l uid (id -u $USER)
    if [ $uid -eq 0 ]
        prompt_segment red white " ! "
        set_color normal
        echo -n -s " "
    else
        prompt_segment normal normal " \$ "
    end

    set_color normal
end

# Show db user and host
function show_db -d "Show the current db user and host"
    if test $PROMPT_DB_VAR
        set -l decomp (string match -r '^.+://(.+?):.+@(.+?)[:/]' $$PROMPT_DB_VAR)
        set -l user $decomp[2]
        set -l host $decomp[3]
        prompt_segment normal grey "DB: [$user@$host]\n"
    end
end

## SHOW PROMPT
function fish_prompt
    set -g RETVAL $status
    show_status
    show_db
    show_nix_shell_info
    show_virtualenv
    show_user
    show_pwd
    show_prompt
end
