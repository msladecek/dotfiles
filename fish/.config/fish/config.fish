if not functions -q fisher
    set -q XDG_CONFIG_HOME; or set XDG_CONFIG_HOME ~/.config
    curl https://git.io/fisher --create-dirs -sLo $XDG_CONFIG_HOME/fish/functions/fisher.fish
    fish -c fisher
end

theme_gruvbox dark hard

set -x EDITOR "nvim"
set -x TERMINAL "konsole"
set -x TERMINFO /usr/share/terminfo

set fish_prompt_pwd_dir_length 0
set fish_greeting
set default_user martin

# direnv
eval (direnv hook fish)
set -x DIRENV_LOG_FORMAT ""

# pyenv
status --is-interactive
and source (pyenv init -|psub)
status --is-interactive
and source (pyenv virtualenv-init -|psub)

# bat
set -x BAT_THEME "OneHalfDark"

function read_confirm
    while true
        read -p 'echo "Confirm? (y/[n]):"' -l confirm

        switch $confirm
            case Y y
                return 0
            case '' N n
                return 1
        end
    end
end

function zi
    z -l $argv | tr -s ' ' | cut -d' ' -f2- | fzf
end

function notes
    nvim ~/stuff/notes.txt
end

function new-branch
    set -l name $argv[1]
    git fetch --all
    git checkout origin/master
    git checkout -b msladecek/$name
    git commit --allow-empty --message="wip: $name"
    git push -u origin msladecek/$name
end

function push-force-with-lease
    set -l commit_msg (git log -1 --pretty=%B)
    if string match --ignore-case 'wip*' "$commit_msg"
        set_color red
        echo "Warning: commit is marked as work-in-progress"
        set_color normal
    end
    git push --force-with-lease
end

function just-fucking-push-everything
    set -l commit_msg (git log -1 --pretty=%B)
    echo "Amending all changes to commit $commit_msg"
    if not read_confirm
        return
    end
    git add --all
    git commit --amend --no-edit --no-verify
    git push --push-option=ci.skip --force-with-lease
end

function amend-all
    set -l me (git config user.email)
    set -l previous (git show -s --format='%ae')
    if [ $previous != $me ]
        echo "Can't amend to a commit of another author ($previous)"
        return
    end
    git add --all
    git commit --amend --no-edit
end

function rebase-onto-master
    git fetch --all
    if git rev-parse --quiet --verify main > /dev/null
        git rebase origin/main
    else
        git rebase origin/master
    end
end

function push-set-upstream-origin
    git push --set-upstream origin (git branch --show-current)
end

function get-gitignore
    curl -L -s https://www.gitignore.io/api/$argv
end

function blacken
    set -l line $argv[1] or 100
    black **.py --line-length=$line
end

abbr -a kube 'kubectx; and kubens; and k9s'
abbr -a glog git log --oneline --graph
abbr -a ghash git rev-parse HEAD
abbr -a jlab jupyter lab
abbr -a dockershell docker run --rm -i -t --entrypoint=/bin/sh
abbr -a clipboard xclip -selection clipboard
abbr -a package-update-ubuntu "sudo apt update ;and sudo apt upgrade ;and sudo apt autoremove"
abbr -a bbrepl rlwrap bb --repl

alias pcp="rsync -r --progress"
alias tmux="tmux -2"
alias reboot="uptime; sleep 3; systemctl reboot"
alias poweroff="uptime; sleep 3; systemctl poweroff"
alias sl="sl -l"

alias portal="bb -cp (clojure -Spath -M:portal/cli) -m portal.main"
alias portal-edn="portal edn"
alias portal-json="portal json"
alias portal-transit="portal transit"
alias portal-yaml="portal yaml"
set -gx VOLTA_HOME "$HOME/.volta"
set -gx PATH "$VOLTA_HOME/bin" $PATH
