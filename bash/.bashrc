export HISTFILESIZE=
export HISTSIZE=

case $TERM in
    (xterm*) set_title='\[\e]0;\w\a\]';;
    (*) set_title= ;;
esac

PROMPT_DIRTRIM=4
# green and blue


if [ -n "$SSH_CLIENT" ]; then
    PS1=$set_title'\[\e[1;36m\]\u@\h \[\e[1;33m\]\w\[\e[0m\] \$ '
else
    PS1=$set_title'\[\e[1;32m\]\u@\h \[\e[1;34m\]\w\[\e[0m\] \$ '
fi


# red and yellow (root)
#PS1='\[\e]0;\w\a\]\[\e[1;31m\]\u@\h \[\e[33m\]\w\[\e[0m\] \$ '

# green and yellow
#PS1='\[\e]0;\w\a\]\[\e[32m\]\u@\h \[\e[33m\]\w\[\e[0m\] \$ '
export EDITOR='nvim'
export BROWSER='firefox'
export TERMINAL='termite'

[[ -f ~/.bash_aliases ]] && . ~/.bash_aliases
[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion


if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    # We have color support; assume it's compliant with Ecma-48
    # (ISO/IEC-6429). (Lack of such support is extremely rare, and such
    # a case would tend to support setf rather than setaf.)
    color_prompt=yes
    else
    color_prompt=
    fi
fi

if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'
 
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi

man() {
    env LESS_TERMCAP_mb=$'\E[01;31m' \
    LESS_TERMCAP_md=$'\E[01;38;5;74m' \
    LESS_TERMCAP_me=$'\E[0m' \
    LESS_TERMCAP_se=$'\E[0m' \
    LESS_TERMCAP_so=$'\E[38;5;246m' \
    LESS_TERMCAP_ue=$'\E[0m' \
    LESS_TERMCAP_us=$'\E[04;38;5;146m' \
    man "$@"
}


# tabtab source for serverless package
# uninstall by removing these lines or running `tabtab uninstall serverless`
[ -f /home/martin/kiwi/iss/node_modules/tabtab/.completions/serverless.bash ] && . /home/martin/kiwi/iss/node_modules/tabtab/.completions/serverless.bash
# tabtab source for sls package
# uninstall by removing these lines or running `tabtab uninstall sls`
[ -f /home/martin/kiwi/iss/node_modules/tabtab/.completions/sls.bash ] && . /home/martin/kiwi/iss/node_modules/tabtab/.completions/sls.bash
# tabtab source for slss package
# uninstall by removing these lines or running `tabtab uninstall slss`
[ -f /home/martin/kiwi/iss/node_modules/tabtab/.completions/slss.bash ] && . /home/martin/kiwi/iss/node_modules/tabtab/.completions/slss.bash