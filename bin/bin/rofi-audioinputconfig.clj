#!/bin/env bb


(defn parse-source-lines [result line]
  (let [section (get result :section [])
        section-indent (get result :section-indent 0)
        indent (inc (str/last-index-of line "\t"))
        [attr value] (str/split (subs line (dec indent)) #": ")]
    (if-not (nil? value)
      (assoc-in result (conj section attr) value)
      (if (< section-indent indent)
        (assoc result
               :section (conj section attr)
               :section-indent indent)
        (assoc result
               :section (conj (pop section) attr)
               :section-indent indent)))))


(defn parse-source [source-str]
  (let [lines (str/split-lines source-str)
        sources (reduce parse-source-lines {} lines)]
    sources))


(defn load-device-and-ports []
  (let [sources (-> (shell/sh "pactl" "list sources")
                    :out
                    (str/split #"\n\n"))]
    (mapv parse-source sources)))


(load-device-and-ports)
