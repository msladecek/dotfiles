#!/usr/bin/env python3

import i3ipc
import click
from dynmen.rofi import Rofi

if __name__ == "__main__":
    i3 = i3ipc.Connection()
    focused_workspace = i3.get_tree().find_focused().workspace().name
    other_outputs = {
        workspace.output
        for workspace in i3.get_workspaces()
        if workspace.visible and not workspace.focused
    }

    if len(other_outputs) == 1:
        target = other_outputs.pop()
    else:
        menu = Rofi(
            prompt=f"Move current workspace [{focused_workspace}] to output",
            case_insensitive=True,
        )
        target = menu(sorted(other_outputs)).selected

    i3.command(f"move workspace to output {target}")
