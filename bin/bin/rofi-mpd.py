#!/usr/bin/env python

from functools import partial

from dynmen.rofi import Rofi
from mpd import MPDClient


class MPD(MPDClient):
    def __init__(self):
        super().__init__()
        self.connect("localhost", 6600)
        self.iterate = True

    @staticmethod
    def _track_format(track):
        fields = ["artist", "album", "title"]
        return " - ".join(str(track.get(field)) for field in fields)

    @property
    def state(self):
        return self.status()["state"]

    @property
    def volume(self):
        return self.status()["volume"]

    @volume.setter
    def volume(self, value):
        value = int(value)
        if not 0 <= value <= 100:
            raise ValueError("Volume must be in range [0 - 100]")
        self.setvol(value)
        return value

    @property
    def _track_prompt(self):
        return (
            f"[{self.state}] " + self._track_format(self.currentsong())
            if self.currentsong()
            else ""
        )

    def play_pause(self):
        if self.state == "play":
            return self.pause()
        return self.play()

    def _menu(self, prompt, options, return_attr="value"):
        menu = Rofi(prompt=prompt, case_insensitive=True)
        return getattr(menu(options), return_attr)

    def select_track(self):
        tracks = {
            self._track_format(track): track["id"] for track in self.playlistinfo()
        }
        track_id = self._menu("Select track", tracks)
        self.playid(track_id)

    def select_playlist(self, replace=True):
        playlists = {
            playlist["playlist"]: playlist["playlist"]
            for playlist in sorted(self.listplaylists(), key=lambda d: d["playlist"].lower())
        }
        playlist = self._menu("Select playlist", playlists)
        if replace:
            self.clear()
        self.load(playlist)
        if replace:
            self.play()

    def playlist_menu(self):
        commands = {
            "replace queue": self.select_playlist,
            "add to queue": partial(self.select_playlist, False),
        }
        command = self._menu(self._track_prompt, commands)
        return command()

    def mpd_audio_menu(self):
        presets = map(str, [25, 50, 75, 100])
        level = self._menu(
            f"Set mpd volume level (currently {self.volume})",
            presets,
            return_attr="selected",
        )
        self.volume = level

    def main_menu(self):
        commands = {
            "play/pause": self.play_pause,
            "play": self.play,
            "pause": self.pause,
            "next track": self.next,
            "previous track": self.previous,
            "select track": self.select_track,
            "playlist...": self.playlist_menu,
            "mpd volume...": self.mpd_audio_menu,
        }

        command = self._menu(self._track_prompt, commands)
        return command()


if __name__ == "__main__":
    client = MPD()
    client.main_menu()
