#!/bin/env bb


(defn parse-display-header-line [line]
  (let [[id state & fields] (str/split line #" ")]
    {:id id
     :is-connected (= "connected" state)
     :is-primary (= "primary" (first fields))}))


(defn parse-display-mode-line [line]
  (let [[shape-str framerate-strs] (-> line str/trim (str/replace #"_[0-9]+" "") (str/split #" +" 2))
        is-interlaced (str/ends-with? shape-str "i")
        shape-str (if is-interlaced (subs shape-str 0 (dec (count shape-str))) shape-str)]
    {:shape (mapv edn/read-string (str/split shape-str #"x"))
     :is-interlaced is-interlaced
     :is-current (boolean (some #{\*} framerate-strs))
     :is-preffered (boolean (some #{\+} framerate-strs))}))


(defn parse-display-string [string]
  (let [[header & modes] (str/split-lines string)
        header-info (parse-display-header-line header)
        modes-info (mapv parse-display-mode-line modes)
        selected-mode (first (concat (filter :is-current modes-info)
                                     (filter :is-preffered modes-info)))]
    (cond-> header-info
      (seq selected-mode) (assoc :shape (:shape selected-mode)))))


(defn parse-xrandr [xrandr-output]
  (let [matching-strings (str/split xrandr-output #"\n(?!   )")
        display-strings (filter (complement #(str/starts-with? % "Screen")) matching-strings)]
    (mapv parse-display-string display-strings)))


(defn read-xrandr-config []
  (->> (shell/sh "xrandr")
       :out
       parse-xrandr
       (filter :is-connected)))


(defn find-matching-presets [config presets]
  (letfn [(match-map [left-map right-map]
            (let [left-keys (set (keys left-map))
                  right-keys (set (keys right-map))
                  left-only-keys (set/difference left-keys right-keys)
                  right-only-keys (set/difference right-keys left-keys)
                  common-keys (set/intersection left-keys right-keys)
                  left-common-values (select-keys left-map common-keys)
                  right-common-values (select-keys right-map common-keys)]
              (when (= left-common-values right-common-values)
                (merge left-common-values
                       (select-keys left-map left-only-keys)
                       (select-keys right-map right-only-keys)))))
          (match-config [config preset]
            (let [matched (map match-map config preset)]
              (when (and (every? (complement nil?) matched)
                         (= (count config) (count preset)))
                matched)))]
    (into {} (for [[preset-name preset-config] presets
                   :let [matched (match-config config preset-config)]
                   :when (not (nil? matched))]
               [preset-name matched]))))


(defn config->xrandr-command [config]
  (letfn [(xrandr-display-params [config]
            (let [params ["--output" (:id config)
                          ; "--mode" (str/join "x" (:shape config))
                          "--pos" (str/join "x" (:offset config))]]
              (cond-> params
                (:is-primary config) (conj "--primary")
                (:target-shape config) (into ["--mode" (str/join "x" (:target-shape config))]))))]
    (into ["xrandr"] (mapcat xrandr-display-params config))))


(defn rofi-select [values]
  (let [cmd (str "echo \"" (str/join "\n" values) "\" | rofi -dmenu")]
    (-> (shell/sh "/bin/sh" "-c" cmd) :out str/trim)))


(let [current-config (read-xrandr-config)
      presets {"top" [{:id "eDP-1" :is-primary true  :shape [1920 1080] :offset [0 1080]}
                      {            :is-primary false :shape [1920 1080] :offset [0 0]}]
               "top-tall" [{:id "eDP-1" :is-primary true  :shape [1920 1200] :offset [0 1080]}
                           {            :is-primary false :shape [1920 1080] :offset [0 0]}]
               "top-right" [{:id "eDP-1" :is-primary true  :shape [1920 1080] :offset [0 820]}
                            {            :is-primary false :shape [1920 1080] :offset [1920 0]}]
               "rikovice" [{:id "eDP-1" :is-primary true  :shape [1920 1080] :offset [0 1200]}
                           {            :is-primary false :shape [1920 1200] :offset [0 0]}]
               "office-dell" [{:id "eDP-1" :is-primary true  :shape [1920 1080] :offset [760 1440]}
                              {            :is-primary false :shape [3440 1440] :offset [0 0]}]
               "office-thinkpad" [{:id "eDP-1" :is-primary true  :shape [1920 1200] :offset [760 1440]}
                                  {            :is-primary false :shape [3440 1440] :offset [0 0]}]
               "office-thinkpad-reshape" [{:id "eDP-1" :is-primary true  :shape [3840 2400] :target-shape [1920 1200] :offset [760 1440]}
                                          {            :is-primary false :shape [3440 1440] :offset [0 0]}]
               "single" [{:id "eDP-1" :is-primary true :shape [1920 1080] :offset [0 0]}]}
      matching-presets (find-matching-presets current-config presets)
      xrandr-command (config->xrandr-command
                      (if (= 1 (count matching-presets))
                        (-> matching-presets vals first)
                        (get matching-presets (rofi-select (keys matching-presets)))))]
  (apply shell/sh xrandr-command))
