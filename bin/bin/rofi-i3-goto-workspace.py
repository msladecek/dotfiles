#!/usr/bin/env python3

import i3ipc
import click
from dynmen.rofi import Rofi
from natsort import natsorted, ns


def unfocused_workspaces(i3):
    return list(
        natsorted(
            (ws.name for ws in i3.get_workspaces() if not ws.focused),
            alg=ns.NUMAFTER,
        )
    )


def goto_workspace(i3, move_client=False):
    menu = Rofi(prompt="Goto workspace" + (" (with active container)" if move_client else ''), case_insensitive=True)
    print('move client', move_client)
    workspaces = unfocused_workspaces(i3)
    selected = menu(workspaces).selected

    if move_client:
        i3.command(f"move container to workspace {selected}")

    i3.command(f"workspace {selected}")


@click.command()
@click.option("--move", is_flag=True)
def cli(move):
    i3 = i3ipc.Connection()
    goto_workspace(i3, move_client=move)


if __name__ == "__main__":
    cli()
