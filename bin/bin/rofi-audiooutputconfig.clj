#!/bin/env bb


(defn parse-sink-str [sink-str]
  (let [lines (str/split-lines sink-str)]
    (reduce (fn [values line]
              (let [[attr value] (str/split line #": ")]
                (case attr
                  "\tName" (assoc values :name value)
                  "\tDescription" (assoc values :description value)
                  values)))
            {}
            lines)))


(defn load-sinks []
  (let [sinks (-> (shell/sh "pactl" "list" "sinks")
                  :out
                  (str/split #"\n\n"))]
    (mapv parse-sink-str sinks)))


(defn rofi-select [sinks]
  (let [options-str (apply str (interpose "\n" (keys sinks)))
        selected (-> (shell/sh "rofi" "-dmenu" "-p" "Select ouptut device" :in options-str)
                     :out
                     str/trim)]
    (sinks selected)))


(defn set-default-sink! [sink-id]
  (shell/sh "pacmd" "set-default-sink" sink-id))


(defn move-existing-streams! [sink-id]
  (let [streams (->> (shell/sh "pactl" "list" "short" "sink-inputs")
                     :out
                     str/split-lines
                     (map #(first (str/split % #"\t"))))]
    (doseq [stream-id streams]
      (shell/sh "pactl" "move-sink-input" stream-id sink-id))))


(let [available-sinks (load-sinks)
      selected-sink-id (rofi-select (into {} (map (juxt :description :name) available-sinks)))]
  (when selected-sink-id
    (set-default-sink! selected-sink-id)
    (move-existing-streams! selected-sink-id)))
