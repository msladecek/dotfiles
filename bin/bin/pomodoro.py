#!/usr/bin/env python

import os
import time
import threading
import socket
import subprocess
from contextlib import suppress
from pathlib import Path

import click

import notify2

SOCK_FILE = str(Path("/tmp/pomodoro-socket"))
ALERT_SOUND_FILE = str(Path("~/dotfiles/resources/alert.wav").expanduser())


class Timer:
    def __init__(self, duration, description=None, when_done=None):
        self.duration = duration
        self.running = False
        self.done = False
        self.description = description
        self.when_done = when_done

    def start(self):
        if not self.running:
            self.start_time = time.time()
            self.stop_time = time.time() + self._time_remaining

            self.timer_thread = threading.Timer(self._time_remaining, self.stop)
            self.timer_thread.start()
            self.running = True

    def pause(self):
        if self.running:
            self.duration = self._time_remaining
            self.running = False
            self.timer_thread.cancel()

    def stop(self, abort=False):
        self.pause()

        if not abort:
            self.duration = 0
            self.done = True
            if callable(self.when_done):
                self.when_done()

    @property
    def _time_remaining(self):
        if self.running:
            return self.stop_time - time.time()
        else:
            return self.duration

    @property
    def clock_remaining(self):
        minutes = int(self._time_remaining // 60)
        seconds = int(self._time_remaining % 60)

        return f"{minutes:02}:{seconds:02}"

    @property
    def status(self):
        if self.done:
            return "DONE"
        elif not self.running:
            return "PAUSED"
        else:
            return "RUNNING"

    def __repr__(self):
        return f"Timer: {self.status} {self.clock_remaining}"


class PomodoroTimer:
    def __init__(
        self,
        work_duration="25:00",
        break_duration="5:00",
        longbreak_duration="15:00",
        n_timers=4,
    ):

        self.n_timers = n_timers
        self.work_duration = self.deformat_time(work_duration)
        self.break_duration = self.deformat_time(break_duration)
        self.longbreak_duration = self.deformat_time(longbreak_duration)
        self.schedule = []
        self._make_schedule()

    def _make_schedule(self):
        for _ in range(self.n_timers):
            self.schedule.append(
                Timer(
                    self.work_duration,
                    description="work",
                    when_done=self._next_timer,
                )
            )
            self.schedule.append(
                Timer(
                    self.break_duration,
                    description="break",
                    when_done=self._next_timer,
                )
            )

        self.schedule.pop(-1)
        self.schedule.append(
            Timer(
                self.longbreak_duration,
                description="long break",
                when_done=self._next_timer,
            )
        )

    def notify(self):
        notification = notify2.Notification("Pomodoro timer", self.status())
        subprocess.run(
            ["aplay", ALERT_SOUND_FILE],
            stdout=subprocess.DEVNULL,
            stderr=subprocess.DEVNULL,
        )

        notification.set_timeout(10000)
        notification.show()


    def _next_timer(self):
        if self.schedule != []:
            self.schedule.pop(0)

        if self.schedule == []:
            self._make_schedule()

        ct = self._current_timer
        self.notify()
        ct.start()

    def start(self):
        self._current_timer.start()

    def pause(self):
        self._current_timer.pause()

    def abort(self):
        self._current_timer.stop(abort=True)

    def toggle(self):
        if self._current_timer.running:
            self._current_timer.pause()
        else:
            self._current_timer.start()

    def skip_break(self):
        ct = self._current_timer
        if ct.description == "break":
            self.schedule[-1].duration += ct.duration
            ct.stop()

    def status(self):
        descdict = {"work": "◎", "break": "| ○", "long break": "|"}
        done = self.n_timers - self.n_timers_left
        ct = self._current_timer
        n_timerstr = done * "◉ "
        n_timerstr += descdict[ct.description] + " "
        n_timerstr += (self.n_timers_left - 1) * "○ "

        if ct.running:
            timeleft = f" {ct.clock_remaining} "
        else:
            timeleft = f"[{ct.clock_remaining}]"

        return f"{n_timerstr} {ct.description} {timeleft}"

    def __str__(self):
        ct = self._current_timer
        return (
            f"PomodoroTimer(n_timers_left={self.n_timers_left}, "
            "task={ct.description}, timer=({ct.timeleft} {ct.status}))"
        )

    @property
    def _current_timer(self):
        return self.schedule[0]

    @property
    def n_timers_left(self):
        return len(self.schedule) // 2

    @staticmethod
    def deformat_time(timestr):
        tlist = timestr.split(":")
        seconds = int(tlist[-1])
        if len(tlist) > 1:
            seconds += 60 * int(tlist[-2])

        return seconds

    def exec_command(self, command):
        # todo: getattr
        command_dict = {
            "status": self.status,
            "toggle": self.toggle,
            "pause": self.pause,
            "start": self.start,
            "skip": self.skip_break,
            "abort": self.abort,
        }

        if command in command_dict:
            return command_dict[command]()



@click.group()
def cli():
    pass


@cli.command()
@click.option(
    "--work",
    default="25:00",
    help="length of work interval",
    show_default=True,
)
@click.option(
    "--break",
    default="05:00",
    help="length of break interval",
    show_default=True,
)
@click.option(
    "--long-break",
    default="15:00",
    help="length of long break interval",
    show_default=True,
)
def init(**kwargs):
    notify2.init("N/A")
    pt = PomodoroTimer(kwargs["work"], kwargs["break"], kwargs["long_break"])

    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    with suppress(OSError):
        os.remove(SOCK_FILE)

    sock.bind(SOCK_FILE)
    sock.listen(1)
    while True:
        conn, addr = sock.accept()
        data = conn.recv(2048).decode()
        if data == "ABORT":
            pt.exec_command("abort")
            break

        data = pt.exec_command(data.strip())
        if data:
            conn.send(data.encode())
        conn.close()

    sock.close()


@cli.command()
def abort():
    send_command("ABORT")


def send_command(command):
    sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
    sock.connect(SOCK_FILE)
    sock.send(command.encode())
    data = sock.recv(2048).decode()
    sock.close()
    return data


@cli.command()
def status():
    try:
        s = send_command("status")
        print(s)
    except (ConnectionRefusedError, FileNotFoundError):
        print()


@cli.command()
def start():
    send_command("start")


@cli.command()
def toggle():
    send_command("toggle")


@cli.command()
def pause():
    send_command("pause")


@cli.command()
def skip():
    send_command("skip")


if __name__ == "__main__":
    cli()
