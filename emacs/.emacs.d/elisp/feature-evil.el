(use-package evil
  :init
  (setq evil-want-integration t
	evil-want-C-u-scroll t
	evil-want-keybinding nil)
  (evil-mode 1)
  :config
  (evil-set-initial-state 'Custom-mode 'motion))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

(use-package evil-commentary
  :after evil
  :config
  (add-hook 'prog-mode-hook 'evil-commentary-mode)
  (add-hook 'conf-mode-hook 'evil-commentary-mode))

(use-package evil-surround
  :after evil
  :config
  (global-evil-surround-mode))

(use-package evil-numbers
  :after evil)

(use-package evil-indent-textobject
  :after evil)

(use-package evil-lion
  :after evil)

(use-package anzu)

(use-package evil-anzu
  :after (evil anzu)
  :config
  (global-anzu-mode +1))

(use-package evil-mc
  :config
  (with-eval-after-load 'smartparens
    (let ((vars (cdr (assq :default evil-mc-cursor-variables))))
      (unless (memq (car sp--mc/cursor-specific-vars) vars)
        (setcdr (assq :default evil-mc-cursor-variables)
		(append vars sp--mc/cursor-specific-vars)))))
  (global-evil-mc-mode +1))


(defhydra hydra-multiple-cursors (:hint nil :color pink)
  "
  ^Mark^                            ^Skip^                              ^Miscellaneous^
--^----^----------------------------^----^------------------------------^-------------^--
  _N_: Mark & Goto previous match   _M-N_: Skip & Goto previous match   _r_: Resume
  _n_: Mark & Goto next match       _M-n_: Skip & Goto next match       _p_: Pause
  _m_: Mark here (if paused)
"
  ("N" evil-mc-make-and-goto-prev-match)
  ("n" evil-mc-make-and-goto-next-match)
  ("m" evil-mc-make-cursor-here)

  ("M-N" evil-mc-skip-and-goto-prev-match)
  ("M-n" evil-mc-skip-and-goto-next-match)

  ("p" evil-mc-pause-cursors)
  ("r" evil-mc-resume-cursors)

  ("TAB" evil-mc-make-all-cursors "Mark all matches & Exit"   :exit t)
  ("q" evil-mc-undo-all-cursors "Remove all cursors & Exit" :exit t)
  )


(general-evil-setup)
(evil-normalize-keymaps)

(provide 'feature-evil)
