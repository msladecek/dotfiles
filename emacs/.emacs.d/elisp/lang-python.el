(use-package python)


(define-derived-mode msla/anaconda-doc-mode help-mode "Anaconda doc")

(defun msla/anaconda-doc-set-mode (buf)
  "Sets the anaconda doc buffer's mode to help-derived one"
  (with-current-buffer buf
    (msla/anaconda-doc-mode))
  buf)

(use-package anaconda-mode
  :init
  (add-hook 'python-mode-hook 'anaconda-mode)
  (add-hook 'python-mode-hook 'anaconda-eldoc-mode)
  (advice-add 'anaconda-mode-documentation-view :filter-return #'msla/anaconda-doc-set-mode)
  :config
  (setq python-shell-interpreter "ipython"
	python-shell-interpreter-args "-i --simple-prompt"))


(use-package company-anaconda
  :config
  (add-to-list 'company-backends 'company-anaconda)
  (eval-after-load 'company
    '(add-to-list 'company-backends '(company-anaconda :with company-capf))))

(use-package pip-requirements
  :config
  (add-hook 'pip-requirements-mode-hook #'pip-requirements-auto-complete-setup))

(use-package pyenv-mode
  :init
  (add-to-list 'exec-path "~/.pyenv/shims")
  (setenv "WORKON_HOME" "~/.pyenv/versions/")
  (defun projectile-pyenv-mode-set ()
    "Set pyenv version matching project name."
    (let ((project (projectile-project-name)))
      (if (member project (pyenv-mode-versions))))
    (pyenv-mode-set project))
  (pyenv-mode-unset)
  :config
  (add-hook 'projectile-after-switch-project-hook 'projectile-pyenv-mode-set))

(use-package ipython-shell-send)

(use-package python-pytest
  :after python
  :custom
  (python-pytest-arguments
   '("--color"          ;; colored output in the buffer
     "--failed-first"))) ;; run the previous failed tests first



;; This doesn't work on signatures with type hints
;;
;; (use-package sphinx-doc
;;   :config
;;   (add-hook 'python-mode-hook (lambda ()
;; 				(require 'sphinx-doc)
;; 				(sphinx-doc-mode t))))

;; (defun pyenv-init()
;;   (setq global-pyenv (replace-regexp-in-string "\n" "" (shell-command-to-string "pyenv global")))
;;   (message (concat "Setting pyenv version to " global-pyenv))
;;   (pyenv-mode-set global-pyenv)
;;   (defvar pyenv-current-version nil global-pyenv))

;; (defun pyenv-activate-current-project ()
;;   "Automatically activates pyenv version if .python-version file exists."
;;   (interactive)
;;   (f-traverse-upwards
;;    (lambda (path)
;;      (message path)
;;      (let ((pyenv-version-path (f-expand ".python-version" path)))
;;        (if (f-exists? pyenv-version-path)
;; 	   (progn
;; 	     (setq pyenv-current-version (s-trim (f-read-text pyenv-version-path 'utf-8)))
;; 	     (pyenv-mode-set pyenv-current-version)
;; 	     (pyvenv-workon pyenv-current-version)
;; 	     (message (concat "Setting virtualenv to " pyenv-current-version))))))))

;; (add-hook 'after-init-hook 'pyenv-init)
;; (add-hook 'projectile-after-switch-project-hook 'pyenv-activate-current-project)
;; (add-hook 'find-file-hook 'pyenv-activate-current-project)


(major-leader-def
  :states '(normal motion)
  :keymaps 'anaconda-mode-map

  "" nil

  "g" '(:ignore t :which-key "goto/jump")
  "gd" '(anaconda-mode-find-definitions              :which-key "find definition")
  "gD" '(anaconda-mode-find-definitions-other-window :which-key "find definition (other window)")
  "gr" '(anaconda-mode-find-references               :which-key "find references")
  "gR" '(anaconda-mode-find-references-other-window  :which-key "find references (other window)")
  "ga" '(anaconda-mode-find-assignments              :which-key "find assignments")
  "gA" '(anaconda-mode-find-assignments-other-window :which-key "find assignments (other window)")

  "t" '(:ignore t :which-key "tags")
  "tt" '(helm-gtags-select   :which-key "select tag")
  "td" '(helm-gtags-find-tag :which-key "find definition")

  "h" '(:ignore t :which-key "help")
  "hh" '(anaconda-mode-show-doc :which-key "find docs at point")

  "b" '(xref-pop-marker-stack  :which-key "return from find")

  "T" '(:ignore t :which-key "test")
  "Tp" 'python-pytest-popup
  "Tt" 'python-pytest
  "Tf" 'python-pytest-file
  "TF" 'python-pytest-file-dwim
  "Tm" 'python-pytest-function
  "TM" 'python-pytest-function-dwim
  "Tl" 'python-pytest-last-failed

  "e" '(:ignore t :which-key "eval")
  "e'" '(run-python                :which-key "run ipython")
  "ed" '(ipython-shell-send-defun  :which-key "eval defun")
  "er" '(ipython-shell-send-region :which-key "eval region")
  "eb" '(ipython-shell-send-buffer :which-key "eval buffer"))


  ;; `spninx-doc' doesn't work on function signatures with type hints
  ;; "i" '(:ignore t :which-key "insert")
  ;; "id" '(sphinx-doc :which-key "add sphinx docstring")

  ;; (which-key-declare-prefixes-for-mode 'python-mode "SPC pt" "Testing")
  ;; (evil-leader/set-key-for-mode 'python-mode
  ;; 				"ptp" 'python-pytest-popup
  ;; 				"ptt" 'python-pytest
  ;; 				"ptf" 'python-pytest-file
  ;; 				"ptF" 'python-pytest-file-dwim
  ;; 				"ptm" 'python-pytest-function
  ;; 				"ptM" 'python-pytest-function-dwim
  ;; 				"ptl" 'python-pytest-last-failed))

(provide 'lang-python)
