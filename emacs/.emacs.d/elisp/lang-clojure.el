(use-package cider
  :config
  (setq nrepl-hide-special-buffers t
        nrepl-log-messages nil
        cider-font-lock-dynamically '(macro core function var)
        cider-overlays-use-font-lock t
        cider-prompt-for-symbol nil
        cider-repl-display-help-banner nil
        cider-repl-history-display-duplicates nil
        cider-repl-history-display-style 'one-line
        cider-repl-history-highlight-current-entry t
        cider-repl-history-quit-action 'delete-and-restore
        cider-repl-history-highlight-inserted-item t
        cider-repl-history-size 1000
        cider-repl-pop-to-buffer-on-connect 'display-only
        cider-repl-result-prefix ";; => "
        cider-repl-print-length 100
        cider-repl-use-clojure-font-lock t
        cider-repl-use-pretty-printing t
        cider-repl-wrap-history nil
	cider-stacktrace-default-filters '(tooling dup)))

(use-package cider-hydra
  :after cider
  :config
  (add-hook 'clojure-mode-hook #'cider-hydra-mode))

(major-leader-def
  :states '(normal motion)
  :keymaps 'clojure-mode-map

  "" nil

  "'" '(cider-jack-in :which-key "jack-in")

  "e" '(:ignore t :which-key "eval")
  "ed" '(cider-eval-defun-at-point      :which-key "eval defun")
  "eD" '(cider-insert-defun-in-repl     :which-key "send defun to repl")
  "ee" '(cider-eval-last-sexp           :which-key "eval last sexp")
  "eE" '(cider-insert-last-sexp-in-repl :which-key "send last sexp to repl")
  "er" '(cider-eval-region              :which-key "eval region")
  "eR" '(cider-insert-region-in-repl    :which-key "send region to repl")
  "eb" '(cider-eval-buffer              :which-key "eval buffer")
  "eu" '(cider-undef                    :which-key "undef")

  "g" '(:ignore t :which-key "goto/jump")
  "gb" '(cider-pop-back :which-key "go back")
  "gg" '(cider-find-var :which-key "find vars")
  "gn" '(cider-find-ns  :which-key "find ns")

  "h" '(:ignore t :which-key "help")
  "hn" '(cider-find-ns      :which-key "find ns")
  "ha" '(cider-apropos      :which-key "apropos")
  "hd" '(cider-doc          :which-key "doc")
  "hg" '(cider-grimoire-web :which-key "grimoire")
  "hj" '(cider-javadoc      :which-key "javadoc")

  "i" '(:ignore t :which-key "inspect")
  "ii" '(cider-inspect             :which-key "inspect")
  "ir" '(cider-inspect-last-result :which-key "inspect last result")

  "m" '(:ignore t :which-key "macro")
  "me" '(cider-macroexpand-1   :which-key "expand one")
  "mE" '(cider-macroexpand-all :which-key "expand all")

  "n" '(:ignore t :which-key "namespace")
  "nn" '(cider-browse-ns :which-key "browse")
  "nN" '(cider-browse-ns-all :which-key "browse all")

  "r" '(:ignore t :which-key "repl")
  "rn" '(cider-repl-set-ns                :which-key "set namespace")
  "rq" '(cider-quit                       :which-key "quit")
  "rr" '(cider-refresh                    :which-key "refresh")
  "rR" '(cider-restart                    :which-key "restart")
  "rb" '(cider-switch-to-repl-buffer      :which-key "open buffer")
  "rc" '(cider-find-and-clear-repl-output :which-key "clear"))


(major-leader-def
  :states '(insert)
  :keymaps 'cider-repl-mode-map
  "S-RET" '(cider-repl-newline-and-indent))


(major-leader-def
  :states '(normal motion)
  :keymaps 'cider-repl-mode-map
  "n" '(cider-repl-set-ns       :which-key "set namespace")
  "q" '(cider-quit              :which-key "quit")
  "r" '(cider-refresh           :which-key "refresh")
  "R" '(cider-restart           :which-key "restart")
  "c" '(cider-repl-clear-output :which-key "clear last")
  "C" '(cider-repl-clear-buffer :which-key "clear all")
  "i" '(cider-interrupt         :which-key "interrupt"))


(provide 'lang-clojure)
