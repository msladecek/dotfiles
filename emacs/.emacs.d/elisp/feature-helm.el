(use-package helm
  :preface
  (setq helm-candidate-number-limit 50
	helm-display-header-line nil
	helm-mode-line-string nil
	helm-ff-auto-update-initial-value nil
	helm-find-files-doc-header nil
	helm-mode-handle-completion-in-region nil
	helm-display-buffer-default-width nil
	helm-display-buffer-default-height 0.25
	helm-imenu-execute-action-at-once-if-one nil
	helm-ff-lynx-style-map nil)
  :init
  (require 'helm-config)
  (setq helm-M-x-fuzzy-match t
	helm-mode-fuzzy-match t
	helm-buffers-fuzzy-matching t
	helm-recentf-fuzzy-match t
	helm-locate-fuzzy-match t
	helm-semantic-fuzzy-match t
	helm-imenu-fuzzy-match t
	helm-completion-in-region-fuzzy-match t
	helm-candidate-number-list 150
	helm-split-window-in-side-p t
	helm-move-to-line-cycle-in-source t
	helm-echo-input-in-header-line t
	helm-autoresize-max-height 0
	helm-autoresize-min-height 20)
  :config
  (setq helm-split-window-in-side-p t
        helm-split-window-default-side 'below
	helm-idle-delay 0.0
	helm-input-idle-delay 0.01
	helm-quick-update t
	helm-ff-skip-boring-files t)
  (helm-mode 1))

(use-package helm-ag
  :config
  (setq helm-ag-fuzzy-match t
	helm-ag-command-option "--hidden --skip-vcs-ignores"))

(use-package helm-projectile
  :commands (helm-projectile-find-file
	     helm-projectile-recentf
	     helm-projectile-switch-project
	     helm-projectile-switch-to-buffer)
  :init
  (setq projectile-completion-system 'helm))

(use-package helm-gtags
  :hook (python-mode . helm-gtags-mode)
  :config
  (setq helm-gtags-path-style 'root
	helm-gtags-auto-update t))

(use-package helm-swoop)

(use-package helm-gitignore)

(use-package helm-themes)

(general-def
  :keymaps 'helm-map
  "C-j"      'helm-next-line
  "C-k"      'helm-previous-line
  "C-l"      'helm-next-source
  "C-h"      'helm-previous-source
  "<escape>" 'helm-keyboard-quit
  "<tab>"    'helm-execute-persistent-action
  )


(provide 'feature-helm)
