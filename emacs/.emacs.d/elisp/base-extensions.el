(use-package hydra)
(use-package general)


;; (use-package auto-package-update
;;    :config
;;    (setq auto-package-update-delete-old-versions t
;;          auto-package-update-interval 7)
;;    (auto-package-update-maybe))

(use-package diminish)

(use-package company
  :general
  (:keymaps 'company-active-map
	    "C-k" 'company-select-previous
	    "C-j" 'company-select-next)
  :config
  (setq company-minimum-prefix-length 1)
  (setq company-dabbrev-downcase 0)
  (setq company-idle-delay 0)
  (add-hook 'after-init-hook 'global-company-mode))

(use-package dumb-jump
  :config
  (setq dumb-jump-selector 'helm))

(use-package dashboard
  :after doom-modeline
  :config
  (setq dashboard-items '((agenda . 5)
			  (recents . 10)
			  (projects . 10)))
  (setq dashboard-banner-logo-title nil
	dashboard-startup-banner 'official)
  (dashboard-setup-startup-hook))

(use-package ace-window)

(use-package ediff
  :config
  (setq ediff-window-setup-function 'ediff-setup-windows-plain)
  (setq-default ediff-highlight-all-diffs 'nil)
  (setq ediff-diff-options "-w"))

(use-package exec-path-from-shell
  :config
  ;; Add GOPATH to shell
  (when (memq window-system '(mac ns))
    (exec-path-from-shell-copy-env "GOPATH")
    (exec-path-from-shell-copy-env "PYTHONPATH")
    (exec-path-from-shell-initialize)))

(use-package projectile
  :init
  (setq projectile-globally-ignored-file-suffixes '(".elc" ".pyc" ".o"))
  :config
  (projectile-mode +1))

(use-package recentf
  :config
  (setq recentf-save-file (recentf-expand-file-name "~/.emacs.d/private/cache/recentf")
	recentf-max-saved-items 200)
  (recentf-mode 1))

(use-package smartparens
  :config
  (require 'smartparens-config)
  (add-hook 'prog-mode-hook 'smartparens-mode))

(use-package origami
  :config
  (add-hook 'prog-mode-hook 'origami-mode))

;; (use-package undo-tree
;;   :config
;;   ;; Remember undo history
;;   (setq
;;    undo-tree-auto-save-history nil
;;    undo-tree-history-directory-alist `(("." . ,(concat temp-dir "/undo/"))))
;;   (global-undo-tree-mode 1))

(use-package which-key
  :config
  (setq which-key-idle-delay 0.1
	which-key-idle-secondary-delay 0.0)
  (which-key-mode))

(use-package windmove)

(use-package yasnippet
  :config
  (yas-global-mode 1)
  (use-package yasnippet-snippets))

(setq initial-scratch-message nil)

(use-package editorconfig
  :ensure t
  :config
  (editorconfig-mode 1))

;; (use-package flycheck
;;   :hook (python-mode . flycheck-mode))

(use-package define-word)

;; (use-package realgud)

;; (use-package ein
;;   :config
;;   (setq ein:use-auto-complete t))

;; (use-package flycheck-pycheckers
;;   :after flycheck
;;   :config
;;   (setq flycheck-pycheckers-venv-root "~/.pyenv/versions/csapi-test")
;;   (add-hook 'flycheck-mode-hook #'flycheck-pycheckers-setup))

(use-package parinfer
  :ensure t
  :bind
  (("C-," . parinfer-toggle-mode))
  :init
  (progn
    (setq parinfer-extensions
          '(defaults       ; should be included.
            pretty-parens  ; different paren styles for different modes.
            evil           ; If you use Evil.
            lispy          ; If you use Lispy. With this extension, you should install Lispy and do not enable lispy-mode directly.
            paredit        ; Introduce some paredit commands.
            smart-tab      ; C-b & C-f jump positions and smart shift with tab & S-tab.
            smart-yank))   ; Yank behavior depend on mode.
    (add-hook 'clojure-mode-hook #'parinfer-mode)
    (add-hook 'emacs-lisp-mode-hook #'parinfer-mode)
    (add-hook 'common-lisp-mode-hook #'parinfer-mode)
    (add-hook 'scheme-mode-hook #'parinfer-mode)
    (add-hook 'lisp-mode-hook #'parinfer-mode)))


(provide 'base-extensions)
