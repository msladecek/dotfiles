(use-package restclient
  :init
  (add-to-list 'auto-mode-alist '("\\.restclient\\'" . restclient-mode)))

(major-leader-def
  :states '(normal motion)
  :keymaps 'restclient-mode-map

  "g" '(:ignore t :which-key "goto/jump")
  "n" '(restclient-jump-next                        :which-key "next")
  "p" '(restclient-jump-prev                        :which-key "prev")
  "s" '(restclient-http-send-current-stay-in-window :which-key "send & stay")
  "S" '(restclient-http-send-current                :which-key "send")
  "," '(restclient-http-send-current                :which-key "send")
  "R" '(restclient-http-send-current-raw            :which-key "send raw")
  "y" '(restclient-copy-curl-command                :which-key "curl"))

(provide 'feature-restclient)
