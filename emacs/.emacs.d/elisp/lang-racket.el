(use-package racket-mode
  :config
  (add-hook 'racket-mode-hook      #'racket-unicode-input-method-enable)
  (add-hook 'racket-repl-mode-hook #'racket-unicode-input-method-enable))

(major-leader-def
  :states '(normal motion)
  :keymaps 'racket-mode-map
  ;; "g" '(:ignore t :which-key "anaconda mode")
  ;; "gd" '(anaconda-mode-find-definitions              :which-key "find definition")
  ;; "gD" '(anaconda-mode-find-definitions-other-window :which-key "find definition (other window)")
  ;; "gr" '(anaconda-mode-find-references               :which-key "find references")
  ;; "gR" '(anaconda-mode-find-references-other-window  :which-key "find references (other window)")
  ;; "ga" '(anaconda-mode-find-assignments              :which-key "find assignments")
  ;; "gA" '(anaconda-mode-find-assignments-other-window :which-key "find assignments (other window)")

  "t" '(:ignore t :which-key "tags")
  "tt" '(helm-gtags-select   :which-key "select tag")
  "td" '(helm-gtags-find-tag :which-key "find definition")

  "r" '(:ignore t :which-key "repl")
  "rr" '(racket-run-and-switch-to-repl :which-key "run in repl")

  ;; "h" '(:ignore t :which-key "help")
  ;; "hh" '(anaconda-mode-show-doc :which-key "find docs at point")

  ;; "b" '(xref-pop-marker-stack  :which-key "return from find")

  ;; `spninx-doc' doesn't work on function signatures with type hints
  ;; "i" '(:ignore t :which-key "insert")
  ;; "id" '(sphinx-doc :which-key "add sphinx docstring")
  )

(provide 'lang-racket)
