;; (use-package sly)

(use-package slime
  :config
  (setq inferior-lisp-program "/usr/bin/sbcl")
  (setq slime-contribs '(slime-fancy))
  ;; (use-package evil-collection
  ;;   :init
  ;;   (setq evil-want-keybinding nil)
  ;;   :config
  ;;   (evil-collection-init 'slime))
  )

(major-leader-def
  :states '(normal motion)
  :keymaps 'lisp-mode-map
  "h" '(:ignore t :which-key "help")
  "ha" '(slime-apropos :which-key "apropos")
  "hh" '(slime-documentation :which-key "find docs at point")

  "R" '(slime-repl :which-key "summon REPL")

  "e" '(:ignore t :which-key "eval")
  "ed" '(slime-eval-defun                   :which-key "eval defun")
  "eb" '(slime-eval-buffer                  :which-key "eval buffer")
  "ee" '(slime-eval-last-expression         :which-key "eval expression")
  "eE" '(slime-eval-last-expression-in-repl :which-key "eval expression in repl")
  )

(provide 'lang-lisp)
