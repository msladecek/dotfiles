(use-package tao-theme
  :ensure t)

;; (use-package minimal-theme
;;   :ensure t)

;; (use-package monochrome-theme
;;   :ensure t)

(use-package doom-themes
  :ensure t
  :config
  (doom-themes-treemacs-config)
  (doom-themes-org-config)
  (load-theme 'doom-one t))

(use-package doom-modeline
  :ensure t
  :config
  (setq doom-modeline-buffer-file-name-style 'truncate-upto-project
	doom-modeline-major-mode-icon nil)
  (setq vc-handled-backends (delq 'Git vc-handled-backends))
  :hook (after-init . doom-modeline-init))

(use-package all-the-icons)

(use-package page-break-lines
  :config
  (global-page-break-lines-mode t))

(use-package rainbow-delimiters
  :config
  (add-hook 'prog-mode-hook 'rainbow-delimiters-mode))

(use-package rainbow-mode)

(use-package hlinum
  :config
  (hlinum-activate))

(use-package linum
  :config
  (setq linum-format " %3d ")
  (setq global-linum-mode nil))

(use-package hl-todo
  :hook (prog-mode . hl-todo-mode))


(with-eval-after-load "tetris"
  (setq tetris-tty-colors (vconcat (mapcar 'doom-color (mapcar 'intern tetris-tty-colors)))))

(add-to-list 'default-frame-alist '(font . "Meslo LG S 12"))

(provide 'base-visuals)
