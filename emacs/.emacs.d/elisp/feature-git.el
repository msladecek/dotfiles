(use-package magit
  :config
  (use-package evil-magit
    :config
    (evil-define-key* evil-magit-state magit-mode-map [escape] nil)))

(use-package magit-popup)

(use-package git-gutter-fringe
  :config
  (setq-default left-fringe-width 10)
  ;; places the git gutter outside the margins.
  (setq-default fringes-outside-margins t)
  ;; thin fringe bitmaps
  (define-fringe-bitmap 'git-gutter-fr:added [224]
    nil nil '(center repeated))
  (define-fringe-bitmap 'git-gutter-fr:modified [224]
    nil nil '(center repeated))
  (define-fringe-bitmap 'git-gutter-fr:deleted [128 192 224 240]
    nil nil 'bottom)
  (global-git-gutter-mode +1)
  ;  :config
  ;; Update git-gutter on focus (in case I was using git externally)
  (add-hook 'focus-in-hook #'git-gutter:update-all-windows)

  (defun +version-control|update-git-gutter (&rest _)
    (when git-gutter-mode
      (ignore (git-gutter))))

  ;; update git-gutter when using magit commands
  (advice-add #'magit-stage-file   :after #'+version-control|update-git-gutter)
  (advice-add #'magit-unstage-file :after #'+version-control|update-git-gutter))

(require 'transient)

(define-key transient-map        (kbd "<escape>") 'transient-quit-one)
(define-key transient-edit-map   (kbd "<escape>") 'transient-quit-one)
(define-key transient-sticky-map (kbd "<escape>") 'transient-quit-seq)

(defhydra hydra-git-gutter (:body-pre (git-gutter-mode 1) :hint nil)
  "
Git gutter:
  _j_: next hunk        _s_: stage hunk     _q_: quit
  _k_: previous hunk    _r_: revert hunk    _Q_: Quit and deactivate git-gutter
  ^ ^                   _p_: popup hunk
  _h_: first hunk
  _l_: last hunk        _R_: set start Revision
"

  ("j" git-gutter:next-hunk)
  ("k" git-gutter:previous-hunk)
  ("h" (progn (goto-char (point-min))
              (git-gutter:next-hunk 1)))
  ("l" (progn (goto-char (point-min))
              (git-gutter:previous-hunk 1)))
  ("s" git-gutter:stage-hunk)
  ("r" git-gutter:revert-hunk)
  ("p" git-gutter:popup-hunk)
  ("R" git-gutter:set-start-revision)
  ("q" nil :color blue)
  ("Q" (progn (git-gutter-mode -1)
              ;; git-gutter-fringe doesn't seem to
              ;; clear the markup right away
              (sit-for 0.1)
              (git-gutter:clear))
   :color blue))


(major-leader-def
  :states '(normal visual motion)
  :keymaps 'with-editor-mode-map
  "," '(with-editor-finish :which-key "confirm commit")
  "q" '(with-editor-cancel :which-key "cancel commit"))



(provide 'feature-git)
