(use-package eyebrowse
  :config
  (setq eyebrowse-new-workspace "*dashboard*")
  (eyebrowse-mode t))

(general-def
  :states '(normal motion insert emacs)
  :keymaps 'override

  "M-TAB" 'eyebrowse-last-window-config
  "M-`"   'eyebrowse-switch-to-window-config
  "M-1"   'eyebrowse-switch-to-window-config-1
  "M-2"   'eyebrowse-switch-to-window-config-2
  "M-3"   'eyebrowse-switch-to-window-config-3
  "M-4"   'eyebrowse-switch-to-window-config-4
  "M-5"   'eyebrowse-switch-to-window-config-5
  "M-6"   'eyebrowse-switch-to-window-config-6
  "M-7"   'eyebrowse-switch-to-window-config-7
  "M-8"   'eyebrowse-switch-to-window-config-8
  "M-9"   'eyebrowse-switch-to-window-config-9
  )

(provide 'feature-eyebrowse)
