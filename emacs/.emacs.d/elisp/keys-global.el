(require 'keys-global-hydra)

(general-def
  :states '(normal motion insert emacs)
  :keymaps 'override

  "M-x" 'helm-M-x

  "M-k" 'windmove-up
  "M-j" 'windmove-down
  "M-h" 'windmove-left
  "M-l" 'windmove-right
  )

(general-def
  :states 'insert
  :keymaps 'override
  "M-c" 'company-complete
  )

(general-def
  :states '(insert normal motion emacs)
  :keymaps 'company-mode-map
  "M-j" 'company-select-next
  "M-k" 'company-select-previous
  "M-c" 'company-abort
  "<delete>" 'company-abort
  )

(general-def
  :states '(normal motion emacs)
  "<escape>" 'keyboard-quit
  "M-;"      'eval-expression
  ";"        'evil-ex
  )

(general-def
  :states '(normal motion)
  :keymaps 'xref--xref-buffer-mode-map
  "ESC" 'evil-delete-buffer
  "q"   'evil-delete-buffer
  )

;; (general-def
;;   :states '(normal motion)
;;   :keymaps 'eldoc-in-minibuffer-mode-map
;;   "" nil
;;   "<escape>" 'evil-delete-buffer
;;   "q"   'evil-delete-buffer
;;   )

;; (general-def
;;   :states '(normal motion)
;;   :keymaps '(text-mode-map prog-mode-map)
;;   "M-<down>" 'evil-scroll-line-down
;;   "M-<up>" 'evil-scroll-line-up
;;   "M-<right>" 'evil-scroll-column-right
;;   "M-<left>" 'evil-scroll-column-left
;;   )

(general-create-definer leader-def
  :prefix "SPC"
  :non-normal-prefix "M-SPC")

(general-create-definer major-leader-def
  :prefix ",")

(leader-def
  :states '(normal motion emacs visual)
  :keymaps 'override

  "" nil

  "/" '(helm-swoop          :which-key "swoop current buffer")
  "k" '(helm-show-kill-ring :which-key "paste from kill ring")

  "p" '(:ignore t :which-key "project")
  "p/" '(helm-projectile-ag               :which-key "search project files")
  "pb" '(helm-projectile-switch-to-buffer :which-key "list project buffers")
  "pf" '(helm-projectile-find-file        :which-key "find file in current project")
  "pp" '(helm-projectile-switch-project   :which-key "list projects")
  "pr" '(helm-projectile-recentf          :which-key "find recent file in project")
  "pt" '(treemacs                         :which-key "treemacs")
  "px" '(projectile-kill-buffers          :which-key "close project buffers")

  "a" '(:ignore t :which-key "applications")
  "ad" '(dired               :which-key "dired")
  "at" '(tetris              :which-key "tetris")
  "au" '(unde-tree-visualize :which-key "undo tree")

  "b" '(:ignore t :which-key "buffer")
  "b/" '(helm-multi-swoop-all :which-key "swoop open buffers")
  "bB" '(bury-buffer          :which-key "bury buffer")
  "bN" '(evil-prev-buffer     :which-key "previous buffer")
  "bb" '(helm-mini            :which-key "list open buffers")
  "bn" '(evil-next-buffer     :which-key "next buffer")
  "bs" '(save-buffer          :which-key "save buffer content")
  "bx" '(evil-delete-buffer   :which-key "close buffer")

  "s" '(:ignore t :which-key "snippet")
  "si" '(yas-insert-snippet :which-key "insert snippet")

  "e" '(:ignore t :which-key "elisp")
  "ei" '(eval-and-replace :which-key "eval expression inline")

  "E" '(:ignore t :which-key "Emacs")
  "Eh" '(switch-to-dashboard :which-key "open home buffer")
  "Es" '(switch-to-scratch   :which-key "open *sratch* buffer")
  "Et" '(helm-themes         :which-key "select theme")
  "Eq" '(evil-quit           :which-key "quit")

  "t" '(:ignore t :which-key "text")
  "tz" '(hydra-zoom/body :which-key "modify text size")
  "tU" '(evil-upcase   :which-key "upcase")
  "tu" '(evil-downcase :which-key "downcase")

  "T" '(:ignore t :which-key "Toggle")
  "Tr" '(rainbow-mode :which-key "rainbow mode")

  "o" '(:ignore t :which-key "org")
  "oc" '(org-capture :which-key "org capture")
  "or" '(helm-org-rifle :which-key "org rifle")

  ;; TODO filter and implement some of these keybindings from spacemacs
  ;; SPC a o # 	org agenda list stuck projects
  ;; SPC a o / 	org occur in agenda files
  ;; SPC a o a 	org agenda list
  ;; SPC a o c 	org capture
  ;; SPC a o e 	org store agenda views
  ;; SPC a o l 	org store link
  ;; SPC a o m 	org tags view
  ;; SPC a o o 	org agenda
  ;; SPC a o O 	org clock out
  ;; SPC a o s 	org search view
  ;; SPC a o t 	org todo list
  ;; SPC C c 	org-capture

  "w" '(:ignore t :which-key "window")
  "ww" '(ace-window          :which-key "switch window")
  "wl" '(windmove-right      :which-key "move right")
  "wh" '(windmove-left       :which-key "move left")
  "wk" '(windmove-up         :which-key "move up")
  "wj" '(windmove-down       :which-key "move bottom")
  "w/" '(split-window-right  :which-key "vertical split")
  "wh" '(split-window-below  :which-key "horizontal split")
  "wv" '(split-window-right  :which-key "vertical split")
  "w-" '(split-window-below  :which-key "horizontal split")
  "w|" '(toggle-window-split :which-key "toggle split")
  "wx" '(delete-window       :which-key "close")

  "W" '(:ignore t :which-key "Workspaces")
  "WW"    '(eyebrowse-switch-to-window-config   :which-key "select workspace")
  "Wr"    '(eyebrowse-rename-window-config      :which-key "rename workspace")
  "Wx"    '(eyebrowse-close-window-config       :which-key "close workspace")
  "W1"    '(eyebrowse-switch-to-window-config-1 :which-key "switch to workspace 1")
  "W2"    '(eyebrowse-switch-to-window-config-2 :which-key "switch to workspace 2")
  "W3"    '(eyebrowse-switch-to-window-config-3 :which-key "switch to workspace 3")
  "W4"    '(eyebrowse-switch-to-window-config-4 :which-key "switch to workspace 4")
  "W5"    '(eyebrowse-switch-to-window-config-5 :which-key "switch to workspace 5")
  "W6"    '(eyebrowse-switch-to-window-config-6 :which-key "switch to workspace 6")
  "W7"    '(eyebrowse-switch-to-window-config-7 :which-key "switch to workspace 7")
  "W8"    '(eyebrowse-switch-to-window-config-8 :which-key "switch to workspace 8")
  "W9"    '(eyebrowse-switch-to-window-config-9 :which-key "switch to workspace 9")
  "W TAB" '(eyebrowse-last-window-config        :which-key "switch to previous workspace")

  "f" '(:ignore t :which-key "file")
  "ff" '(helm-find-files :which-key "open file")
  "fr" '(helm-recentf    :which-key "open recent file")

  "g" '(:ignore t :which-key "git")
  "gc" '(magit-checkout        :which-key "checkout")
  "gh" '(hydra-git-gutter/body :which-key "hunk hydra")
  "gs" '(magit-status          :which-key "magit status")
  "gg" '(magit-status          :which-key "magit status")
  "ge" '(magit-ediff-dwim      :which-key "ediff")

  "h" '(:ignore t :which-key "help")
  "ha" '(helm-apropos    :which-key "apropos")
  "hc" '(customize-group :which-key "customize group")
  "hh" '(help-command    :which-key "help-command")

  "hd" '(:ignore t :which-key "describe")
  "hdb" '(describe-bindings      :which-key "bindings")
  "hdf" '(helm-describe-function :which-key "function")
  "hdk" '(describe-key           :which-key "key")
  "hdm" '(describe-mode          :which-key "mode")
  "hdv" '(helm-describe-variable :which-key "variable")

  "c" '(:ignore t :which-key "code")
  "cl" '(evil-commentary-line   :which-key "comment line")
  "cs" '(yas-insert-snippet     :which-key "insert snippet")
  "cp" '(hydra-smartparens/body :which-key "smartparens")
  "cf" '(hydra-folding/body     :which-key "folding")

  "t" '(:ignore t :which-key "text")
  "tg" '(ace-jump-char-mode          :which-key "jump to char")'
  "ta" '(evil-lion-left              :which-key "align (left)")
  "tA" '(evil-lion-right             :which-key "align (right)")
  "tc" '(hydra-multiple-cursors/body :which-key "multiple cursors")
  "ti" '(helm-imenu                  :which-key "imenu")
  "t+" '(evil-numbers/inc-at-pt      :which-key "increment at point")
  "t-" '(evil-numbers/dec-at-pt      :which-key "decrement at point")
  )

(provide 'keys-global)
