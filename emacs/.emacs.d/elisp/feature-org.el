(use-package org
  :init
  (require 'org-tempo)
  :config
  ;; (setq org-src-tab-acts-natively t)
  (setq org-directory "~/org"))

(use-package deft
  :bind ("<f8>" . deft)
  :commands (deft)
  :config
  (setq deft-extensions '("txt" "md" "org")
	deft-directory "~/misc/text"
	deft-recursive t
	deft-use-filename-as-title t))


(use-package org-bullets
  :config
  (setq org-hide-leading-stars t)
  (add-hook 'org-mode-hook
	    (lambda ()
	      (org-bullets-mode t)
	      (org-indent-mode t))))

;; (use-package helm-org-rifle
;;   :after org)

(use-package evil-org
  :ensure t
  :after org
  :config
  (add-hook 'org-mode-hook 'evil-org-mode)
  (add-hook 'evil-org-mode-hook
            (lambda ()
              (evil-org-set-key-theme)))
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))


(setq org-agenda-files
      '("~/gtd/inbox.org"
	"~/gtd/gtd.org"
	"~/gtd/tickler.org"))

(setq org-refile-targets
      '(("~/gtd/gtd.org" :maxlevel . 3)
	("~/gtd/someday.org" :level . 1)
	("~/gtd/tickler.org" :maxlevel . 2)))

(setq org-capture-templates
      '(("t" "Todo [inbox]" entry
	 (file+headline "~/gtd/inbox.org" "Tasks")
	 "* TODO %i%?")
	("T" "Tickler" entry
	 (file+headline "~/gtd/tickler.org" "Tickler")
	 "* %i%? \n %U")))
            ;; ("w" "Word" entry
            ;;  (file+headline "~/org/words.org" "Words")
            ;;  "* %i%?")


(setq org-todo-keywords
      '((sequence "TODO(t)" "WAITING(w)" "|" "DONE(d)" "CANCELLED(c)")))

;; C-c C-c runs the command org-capture-finalize (found in
;; org-capture-mode-map), which is an interactive compiled Lisp function
;; in ‘~/msla.emacs.d/elpa/org-9.1.14/org-capture.el’.

(general-def
  :states '(normal motion)
  :keymaps 'org-agenda-mode-map
  ":"     'org-agenda-set-tags
  "a"     'org-agenda
  "d"     'org-agenda-deadline
  "f"     'org-agenda-set-effort
  "I"     'org-agenda-clock-in
  "O"     'org-agenda-clock-out
  "P"     'org-agenda-set-property
  "q"     'org-agenda-refile
  "Q"     'org-agenda-clock-cancel
  "s"     'org-agenda-schedule
  "RET"   'org-agenda-goto
  "M-RET" 'org-agenda-show-and-scroll-up)


(major-leader-def
  :states '(normal visual motion)
  :keymaps 'org-capture-mode-map
  "," '(org-capture-finalize :which-key "confirm")
  "q" '(org-capture-kill     :which-key "cancel")
  "r" '(org-capture-refile   :which-key "refile"))


(major-leader-def
  :states '(normal visual motion)
  :keymaps 'org-mode-map
  ","   '(org-ctrl-c-ctrl-c           :which-key "C-c C-c")
  "*"   '(org-ctrl-c-star             :which-key "C-c *")
  "RET" '(org-ctrl-c-ret              :which-key "C-c RET")
  "-"   '(org-ctrl-c-minus            :which-key "C-c -")
  "'"   '(org-edit-special            :which-key "org-edit-special")
  "^"   '(org-sort                    :which-key "org-sort")
  ":"   '(org-set-tags                :which-key "org-set-tags")
  "/"   '(org-sparse-tree             :which-key "org-sparse-tree")
  "."   '(org-time-stamp              :which-key "org-time-stamp")
  "!"   '(org-time-stamp-inactive     :which-key "org-time-stamp-inactive")
  "a"   '(org-agenda                  :which-key "org-agenda")
  "b"   '(org-tree-to-indirect-buffer :which-key "org-tree-to-indirect-buffer")
  "A"   '(org-archive-subtree         :which-key "org-archive-subtree")
  "c"   '(org-capture                 :which-key "org-capture")
  "C"   '(evil-org-recompute-clocks   :which-key "evil-org-recompute-clocks")
  "d"   '(org-deadline                :which-key "org-deadline")
  "D"   '(org-insert-drawer           :which-key "org-insert-drawer")
  "e"   '(org-export-dispatch         :which-key "org-export-dispatch")
  "f"   '(org-set-effort              :which-key "org-set-effort")
  "I"   '(org-clock-in                :which-key "org-clock-in")
  "l"   '(org-open-at-point           :which-key "org-open-at-point")
  "n"   '(org-narrow-to-subtree       :which-key "org-narrow-to-subtree")
  "N"   '(widen                       :which-key "widen")
  "O"   '(org-clock-out               :which-key "org-clock-out")
  "P"   '(org-set-property            :which-key "org-set-property")
  "q"   '(org-clock-cancel            :which-key "org-clock-cancel")
  "r"   '(org-refile                  :which-key "org-refile")
  "s"   '(org-schedule                :which-key "org-schedule")
  "t"   '(org-todo                    :which-key "org-todo")
  "T"   '(org-show-todo-tree          :which-key "org-show-todo-tree")
  "L"   '(org-shiftright              :which-key "org-shiftright")
  "H"   '(org-shiftleft               :which-key "org-shiftleft")
  "K"   '(org-shiftup                 :which-key "org-shiftup")
  "J"   '(org-shiftdown               :which-key "org-shiftdown")

  "S" '(:ignore t :which-key "Subtree")
  "Sl" '(org-demote-subtree    :which-key "org-demote-subtree")
  "Sh" '(org-promote-subtree   :which-key "org-promote-subtree")
  "Sk" '(org-move-subtree-up   :which-key "org-move-subtree-up")
  "Sj" '(org-move-subtree-down :which-key "org-move-subtree-down"))

;; SPC m C-S-l 	org-shiftcontrolright
;; SPC m C-S-h 	org-shiftcontrolleft
;; SPC m C-S-j 	org-shiftcontroldown
;; SPC m C-S-k 	org-shiftcontrolup
;; SPC s j 	spacemacs/jump-in-buffer (jump to a heading)


(provide 'feature-org)
