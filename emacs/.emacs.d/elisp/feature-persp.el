(use-package persp-mode
  :config
  (setq wg-morph-on nil)
  (setq persp-autokill-buffer-on-remove 'kill-weak)
  (add-hook 'after-init-hook #'(lambda () (persp-mode 1))))

(use-package persp-projectile
  :after persp-mode)

(general-def
  :states '(normal motion insert emacs)
  :keymaps 'override

  "M-`"       'persp-switch
  "M-TAB"     'persp-switch-last
  "M-<right>" 'persp-next
  "M-<left>"  'persp-prev
  )

(provide 'feature-persp)
