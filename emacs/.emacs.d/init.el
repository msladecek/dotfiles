(require 'package)
(let* ((no-ssl (and (memq system-type '(windows-nt ms-dos))
                    (not (gnutls-available-p))))
       (proto (if no-ssl "http" "https")))
  (when no-ssl
    (warn "\
Your version of Emacs does not support SSL connections,
which is unsafe because it allows man-in-the-middle attacks.
There are two things you can do about this warning:
1. Install an Emacs version that does support SSL and be safe.
2. Remove this warning from your init file so you won't see it again."))
  ;; Comment/uncomment these two lines to enable/disable MELPA and MELPA Stable as desired
  (add-to-list 'package-archives (cons "melpa" (concat proto "://melpa.org/packages/")) t)
  ;;(add-to-list 'package-archives (cons "melpa-stable" (concat proto "://stable.melpa.org/packages/")) t)
  (when (< emacs-major-version 24)
    ;; For important compatibility libraries like cl-lib
    (add-to-list 'package-archives (cons "gnu" (concat proto "://elpa.gnu.org/packages/")))))

(package-initialize)

(add-to-list 'load-path (concat user-emacs-directory "elisp"))

(require 'base)
(require 'base-visuals)
(require 'base-extensions)
(require 'base-functions)

(require 'keys-global)

(require 'feature-evil)
(require 'feature-helm)
(require 'feature-git)
;; (require 'feature-persp)
(require 'feature-eyebrowse)
;(require 'feature-treemacs)
(require 'feature-org)
(require 'feature-restclient)

(require 'lang-fish)
(require 'lang-clojure)
(require 'lang-python)
;(require 'lang-lisp)
;(require 'lang-racket)
(require 'lang-markdown)
(require 'lang-yaml)
